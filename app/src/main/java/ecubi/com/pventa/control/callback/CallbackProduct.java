package ecubi.com.pventa.control.callback;

import ecubi.com.pventa.model.sugar.groupProduct;
import ecubi.com.pventa.model.sugar.productData;

/**
 * Created by pro on 18/01/18.
 */
public interface CallbackProduct {

    void ClickDelete(productData groupProductItem);
    void ClickEdit(productData groupProductItem);
}
