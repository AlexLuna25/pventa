package ecubi.com.pventa.control.callback.groupProduct;

import ecubi.com.pventa.model.sugar.groupProduct;

/**
 * Created by pro on 18/01/18.
 */
public interface CallbackGroup {

    void ClickDelete(groupProduct groupProductItem);
    void ClickEdit(groupProduct groupProductItem);
}
