package ecubi.com.pventa.control.callback.user;

import ecubi.com.pventa.model.sugar.groupProduct;
import ecubi.com.pventa.model.sugar.user;

/**
 * Created by pro on 18/01/18.
 */
public interface CallbackUser {

    void ClickDelete(user userItem);
    void ClickEdit(user userItem);
}
