package ecubi.com.pventa.library;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import ecubi.com.appecubi.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;



/**
 * Created by Alejandro
 */
public class Utils {

    /*
    * cierra el teclado
    * */
    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /*
    * regresa la fecha actual con el formato en especifico
    * */
    public static String currentDateFormatter()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM");
        String date = dateFormat.format(new Date());
        return date;
    }


    /*
    * realiza una llamada telefónica al telefono proporcionado como paramtero
    * */
    public static void callActionPhone(Context context, String phone) {
        if (phone != null && !phone.isEmpty()) {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + phone));
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            context.startActivity(intent);
        }else{
            Alerts.showToastMessage(context, R.string.number_phone_call_sr);
        }
    }


    /*
    * Regrsa la fecha actual  con el formato en especifico
    * */
    public static String getCurrentTime(){
        DateFormat df = DateFormat.getTimeInstance();
        df.setTimeZone(TimeZone.getTimeZone("gmt"));
        String gmtTime = df.format(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss" );
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(new Date());
    }



    /*
    * Regrsa la fecha actual  con el formato en especifico en Mexicosin hora
    * */
    public static String getCurrentTimeCityDate(){
        return (new SimpleDateFormat("dd-MM-yyyy HH:mm:ss")).format(new Date());
    }

    /*
    * Regrsa la fecha actual  con el formato en especifico en Mexicosin hora
    * */
    public static String getCurrentTimeCityDate2(){
        return (new SimpleDateFormat("dd-MM-yyyy")).format(new Date());
    }

    /*
    * Regrsa la fecha actual  con el formato en especifico
    * */
    public static String getCurrentTimeTwo(){
        DateFormat df = DateFormat.getTimeInstance();
        df.setTimeZone(TimeZone.getTimeZone("gmt"));
        String gmtTime = df.format(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat( "dd-MM-yyyy" );
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(new Date());
    }

    /*obtiene solol la fecha con el formato año-mes-dia*/
    public static String getCurrentDate(){
        DateFormat df = DateFormat.getTimeInstance();
        df.setTimeZone(TimeZone.getTimeZone("gmt"));
        String gmtTime = df.format(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(new Date());
    }

    /*
    * metodo para saber si en un texto contiene una palabra en específico*/
    public static boolean containWordInText(String text,String word){
        return text.toLowerCase().contains(word.toLowerCase());
    }




    public static void setListViewHeightBasedOnChildrenAdapter(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
                view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    /*regresa el nombre de la imagen conforme la fecha y hora actual*/
    public static String getNamePicture(){
        //String timeStamp = new SimpleDateFormat("yyyy_MM_dd_T_HH_mm_ss").format(new Date());
        String timeStamp = getTimeNow();
        return  timeStamp;
    }


    /*genera nombre para la imagen capturada para no tener un nombre repetido*/
    public static String getTimeNow(){
        String year = new SimpleDateFormat("yyyy").format(new Date());
        String month = new SimpleDateFormat("MM").format(new Date());
        String day = new SimpleDateFormat("dd").format(new Date());
        String hour = new SimpleDateFormat("HH").format(new Date());
        String minutes = new SimpleDateFormat("mm").format(new Date());
        String seconds = new SimpleDateFormat("ss").format(new Date());
        return year + "_" + month + "_" + day + "_T_" + hour + "_" + minutes + "_" + seconds;
    }


    /*regresa el nombre del zip con la extención .zip*/
    public static String getNameZIP(String nameZip){
        return  nameZip + ".zip";
    }


    /*Formatea el texto en fecha y regresa la fecha en milis*/
    public static long getDayDateService(String dateStr) {

        DateTimeFormatter formatter = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss 'GMT'")
                .withZoneUTC().withLocale(Locale.US);
        DateTime dt = new DateTime() ;

        try {

            dt = formatter.parseDateTime(dateStr);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return dt.getMillis() ;
    }

    /*regresa la fecha en año mes y dia  sin hora*/
    public static String getCurretDate(){

        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
        String date = sdf.format(new Date());

        return date+"T00:00:00";
    }


    /*regresa la fecha en año mes y dia  sin hora*/
    public static String getCurretOnlyDate(){

        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
        String date = sdf.format(new Date());
        return date;
    }



    /*regresa el texto formateado en año mes  y dia*/
    public static String formatterDayString(String date){
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat( "dd 'de' MMMM");
        String dayParser = "";
        try {
            dt = formatter.parseLocalDate(date).toDate();
            dayParser = sdf.format(dt);
        }catch (Exception e){
            e.printStackTrace();
        }
        return dayParser;
    }

    /*
   * Regrsa la fecha actual  con el formato en especifico en Mexico
   * */
    public static String getCurrentTimeCity(){
        return (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).format(new Date());
    }

    /*regresa el texto formateado en año mes  y dia*/
    public static String formatterDay2String(String date){
        String dayParser="";
        try {

            dayParser = (new SimpleDateFormat( "dd'/'MM'/'yyyy HH':'mm':'ss")).format((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())).parse(date.replace("T"," ")));
        }catch (Exception e){
        }
        return dayParser;
    }


    /*regresa el texto formateado en año mes  y dia*/
    public static String formatterDay3String(String date){
        String dayParser="";
        try {

            dayParser = (new SimpleDateFormat( "yyyy'-'MM'-'dd HH':'mm':'ss")).format((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())).parse(date.replace("T"," ")));
        }catch (Exception e){
        }
        return dayParser;
    }

    public static boolean detectIsMobil(Context context)
    {
        TelephonyManager manager = (TelephonyManager)context.getSystemService(context.TELEPHONY_SERVICE);
        if(manager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
            return false;
        }else{
            return true;
        }
    }

}