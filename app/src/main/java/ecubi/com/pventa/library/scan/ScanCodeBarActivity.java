package ecubi.com.pventa.library.scan;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;

import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

import java.util.List;

import ecubi.com.appecubi.R;
import ecubi.com.pventa.background.sessions.GlobalSession;

public class ScanCodeBarActivity extends AppCompatActivity {

    private CaptureManager capture;
    private CompoundBarcodeView barcodeScannerView;
    GlobalSession Globalsession;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_code_bar);

        barcodeScannerView = (CompoundBarcodeView)findViewById(R.id.zxing_barcode_scanner);

        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.decode();
        barcodeScannerView.decodeContinuous(callback);
        Globalsession=new GlobalSession(this);

    }

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {

            if (result.getText() != null) {

                if (result.getResultMetadata() != null) {
                        barcodeScannerView.setStatusText(result.getText());
                        String codeBar = result.getText() + result.getResultMetadata().get(ResultMetadataType.UPC_EAN_EXTENSION);

                        barcodeScannerView.getBarcodeView().stopDecoding();

                        Globalsession.SetDataCodeBar(result.getText());
                       /* mBus.getDefault().post(new EventPreguntaResponse(getQuestion(), codeBar));

                        mBus.getDefault().postSticky(new BarCodeEvent(question, result.getText(),result.getResultMetadata().get(ResultMetadataType.UPC_EAN_EXTENSION).toString()));

                        mBus.getDefault().post(new CancellEvent(codeBar));*/

                    finish();

                }
            }

        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
            Log.d("ScanCodeBarActivity", "resultpoints " + resultPoints);
        }
    };



    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
        //mBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                barcodeScannerView.setTorchOff();
                return true;

            case KeyEvent.KEYCODE_VOLUME_UP:
                barcodeScannerView.setTorchOn();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
