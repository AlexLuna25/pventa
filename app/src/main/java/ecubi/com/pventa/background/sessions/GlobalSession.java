package ecubi.com.pventa.background.sessions;

import android.content.Context;
import android.content.SharedPreferences;

import ecubi.com.pventa.library.Constants;

/**
 * Created by pro on 17/03/17.
 */
public class GlobalSession {
    /*var to save Session*/
    private SharedPreferences.Editor EditorSesion;

    //View manipulate Var session
    private Context ViewContext;

    //var to check if exist a open session
    private SharedPreferences VarSession;

    public  String FIREBASE_URL = "https://roomvea.firebaseio.com/";



    /*
    * Summary: constructor*/
    public GlobalSession(Context context)
    {
        this.ViewContext=context;
		/* sharepreferences  for my data on session*/
        VarSession= context.getSharedPreferences(Constants.MySession,Context.MODE_PRIVATE);
    }


    /*
    * Summary: almacena  y regrresa la sesisón iniciada*/
    public void SetDataLogin(String set_login)
    {
        EditorSesion = VarSession.edit();
        EditorSesion.putString(Constants.MySession, set_login);
        EditorSesion.commit();
    }
    public String GetDataLogin()
    {
        return  VarSession.getString(Constants.MySession, "");
    }






    /*
      * Summary: almacena el codiog de barras*/
    public void SetDataCodeBar(String set_code_bar)
    {
        EditorSesion = VarSession.edit();
        EditorSesion.putString(Constants.MyCodeBar, set_code_bar);
        EditorSesion.commit();
    }
    public String GetDataCodeBar()
    {
        return  VarSession.getString(Constants.MyCodeBar, "");
    }



    /*
    * Summary: limpia la session del usuario que se logueo*/
    public void ClearSession()
    {
        SetDataLogin("");
    }

}
