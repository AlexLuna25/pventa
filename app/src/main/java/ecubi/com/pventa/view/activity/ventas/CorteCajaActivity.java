package ecubi.com.pventa.view.activity.ventas;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.library.Alerts;
import ecubi.com.pventa.library.Utils;
import ecubi.com.pventa.model.sugar.CorteCaja;
import ecubi.com.pventa.model.sugar.ListproductData;
import ecubi.com.pventa.model.sugar.ventaRealizada;
import ecubi.com.pventa.view.adapter.AdapterViewCorteCaja;

public class CorteCajaActivity extends AppCompatActivity {

    @Bind(R.id.ListViewData)
    RecyclerView mListViewData;

    @Bind(R.id.periodo_uno)
    EditText periodo_uno_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_corte_caja);


        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        setTitle("Corte de caja");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        mListViewData.setLayoutManager(linearLayoutManager);
        mListViewData.setHasFixedSize(true);

        setRenderVentas();
    }

    List<ventaRealizada> ListventaRealizada;
    public void setRenderVentas()
    {
        List<CorteCaja> ListCorteCaja=CorteCaja.find(CorteCaja.class,"fecha=?", Utils.getCurrentTimeCityDate2() + "");
        ListventaRealizada=new ArrayList<>();
        if(ListCorteCaja.size()>0)
        {
            ListventaRealizada=ventaRealizada.find(ventaRealizada.class,"id_Corte_Caja=?",ListCorteCaja.get(0).getId()+"");
        }

        if(ListventaRealizada.size()>0)
        {
            AdapterViewCorteCaja dap=new AdapterViewCorteCaja(this,ListventaRealizada);
            mListViewData.setAdapter(dap);
            double countvalue=0;
            for(ventaRealizada ventaRealizadaItem:ListventaRealizada)
            {
                countvalue+=Double.parseDouble(ventaRealizadaItem.getPrecioProductos().replace("$ ",""));
            }
            periodo_uno_.setText("$ "+countvalue+"");
        }else
        {
            mListViewData.setAdapter(null);
        }
    }


    @OnClick(R.id.corte_caja)
    public void corte_caja()
    {
        finish();
       /* Alerts.showDialogWithCallback(CorteCajaActivity.this, "¿Deseas realizar el corte de caja?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                CorteCaja cort=new CorteCaja();
                cort.setFecha(Utils.getCurrentTimeCityDate2());
                long idCorte=cort.save();
                for(ventaRealizada ventaRealizadaItem:ListventaRealizada)
                {
                    ventaRealizadaItem.setIdCorteCaja(idCorte+"");
                    ventaRealizadaItem.setCorteCaja(true);
                    ventaRealizadaItem.save();
                }

                Alerts.showToastMessage(CorteCajaActivity.this,"Corte de caja realizado correctamente");
                finish();
            }
        });*/
    }



    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
