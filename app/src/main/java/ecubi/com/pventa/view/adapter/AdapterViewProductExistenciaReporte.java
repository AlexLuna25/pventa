package ecubi.com.pventa.view.adapter;

import android.content.Context;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.control.callback.CallbackProduct;
import ecubi.com.pventa.model.sugar.groupProduct;
import ecubi.com.pventa.model.sugar.productData;


public class AdapterViewProductExistenciaReporte extends RecyclerView.Adapter<AdapterViewProductExistenciaReporte.ListProduct>
{

    Context context;
    OnItemClickListener clickListener;
    OnItemLongClickListener longListener;




    List<productData> ListproductData;
    //build to list with image, title and description
    public AdapterViewProductExistenciaReporte(Context context, List<productData> ListproductData)
    {
        this.ListproductData=ListproductData;
        this.context = context;
    }


    @Override
    public ListProduct onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_product_existencia_reporte_item, viewGroup, false);
        return new ListProduct(view);
    }


    public ListProduct ListProduct_;
    @Override
    public void onBindViewHolder(final ListProduct ListProduct_, final int i)
    {
        this.ListProduct_=ListProduct_;
        File f = new File(Environment.getExternalStorageDirectory() + "/Pventa/" + ListproductData.get(i).getImagen());
        Picasso.with(context).load(f).into(ListProduct_.ImageProfileState);

        ListProduct_.mClaveSat.setText(ListproductData.get(i).getDescripcion());

        ListProduct_.price_product.setText(ListproductData.get(i).getClave());

        ListProduct_.mGrupo.setText(ListproductData.get(i).getExistencia()+"");



    }




    @Override
    public int getItemCount()
    {
        return ListproductData == null ? 0 : ListproductData.size();
    }

    public interface OnItemClickListener
    {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener itemClickListener)
    {
        this.clickListener = itemClickListener;
    }


    public interface OnItemLongClickListener
    {
        public void SetOnItemLongClickListener(View view, int position);
    }

    public void SetOnItemLongClickListener(final OnItemLongClickListener longListener)
    {
        this.longListener = longListener;
    }


    class ListProduct extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
    {

        CircleImageView ImageProfileState;
        TextView mClaveSat;
        TextView mGrupo;

        TextView price_product;

        //de.hdodenhof.circleimageview.CircleImageView
        public ListProduct(View itemView)
        {
            super(itemView);
            //Initialize data
            ImageProfileState = (CircleImageView) itemView.findViewById(R.id.profile_image);
            mClaveSat =(TextView) itemView.findViewById(R.id.clave_sat);
            mGrupo=(TextView) itemView.findViewById(R.id.grupo);


            price_product=(TextView) itemView.findViewById(R.id.price_product);

        }

        @Override
        public void onClick(View v)
        {
            clickListener.onItemClick(v, getPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            try{
                longListener.SetOnItemLongClickListener(v, getPosition());
            }catch (Exception g)
            {

            }

            return false;
        }
    }

}


