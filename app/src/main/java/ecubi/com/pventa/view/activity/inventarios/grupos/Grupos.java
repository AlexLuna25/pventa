package ecubi.com.pventa.view.activity.inventarios.grupos;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.control.callback.groupProduct.CallbackGroup;
import ecubi.com.pventa.library.Alerts;
import ecubi.com.pventa.library.Utils;
import ecubi.com.pventa.model.sugar.groupProduct;
import ecubi.com.pventa.view.adapter.AdapterGroup;

public class Grupos extends AppCompatActivity implements CallbackGroup{

    @Bind(R.id.ImageT)ImageView ImageT;
    @Bind(R.id.name_group)
    EditText name_group;
    @Bind(R.id.save_group)
    Button saveGroup;
    @Bind(R.id.ListViewData)
    RecyclerView mListViewData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupos_portrait);
        if(Utils.detectIsMobil(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_grupos_portrait);
        }else
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_grupos_landscape);
        }

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextDrawable drawable = TextDrawable.builder()
                .buildRound("1", Color.rgb(249, 172, 113));
        ImageT.setImageDrawable(drawable);


        setTitle("Grupos");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        mListViewData.setLayoutManager(linearLayoutManager);
        mListViewData.setHasFixedSize(true);
        groupProductItemUpdate=null;
        setInserData();
    }


    public void setInserData()
    {
        List<groupProduct> ListgroupProduct=groupProduct.listAll(groupProduct.class);

        if(ListgroupProduct.size()>0) {
            AdapterGroup adapterGroup=new AdapterGroup(this,ListgroupProduct,this);
            mListViewData.setAdapter(adapterGroup);
        }else
        {
            mListViewData.setAdapter(null);
        }

    }


    @OnClick(R.id.save_group)
    public void save_group()
    {
        String nameGroup =name_group.getText().toString().trim();

        if(nameGroup.isEmpty())
        {
            Alerts.showToastMessage(this,"Necesitas ingresar el nombre del grupo");
            return;
        }
        if(groupProductItemUpdate==null) {
            groupProduct groupProductItem = new groupProduct();
            groupProductItem.setName(nameGroup);
            groupProductItem.save();


            Alerts.showToastMessage(this, "Grupo almacenado correctamente");
        }else
        {
            groupProductItemUpdate.setName(nameGroup);
            groupProductItemUpdate.save();
            groupProductItemUpdate=null;
        }
        name_group.setText("");
        saveGroup.setText("Almacenar");
        setInserData();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_back, menu);
        return true;
    }

    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void ClickDelete(final groupProduct groupProductItem) {
        Alerts.showDialogWithCallback(Grupos.this, "¿Deseas eliminar el grupo "+groupProductItem.getName()+"?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                groupProductItem.delete();
                setInserData();
            }
        });
    }



    groupProduct groupProductItemUpdate;
    @Override
    public void ClickEdit(final groupProduct groupProductItem) {
        Alerts.showDialogWithCallback(Grupos.this, "¿Deseas editar el grupo "+groupProductItem.getName()+"?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                groupProductItemUpdate=groupProductItem;
                name_group.setText(groupProductItem.getName());
                saveGroup.setText("Actualizar");
            }
        });
    }
}
