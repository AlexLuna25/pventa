package ecubi.com.pventa.view.adapter;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ecubi.com.appecubi.R;
import butterknife.Bind;
import butterknife.ButterKnife;
import ecubi.com.pventa.model.sugar.productData;

/**
 * Created by Alejandro
 */
public class AdapterVentaProductoData extends ArrayAdapter<productData> {

    private Context mContext;
    private List<productData> mListAnswer;
    private boolean isCheck;
    int cont = 0,c = 0, co = 0;

    public AdapterVentaProductoData(Context context, int resource, List<productData> Answers) {
        super(context, resource, Answers);
        this.mContext = context;
        this.mListAnswer = Answers;
    }


    @Override
    public int getCount() {
        return mListAnswer.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            Log.d("SYNCADAPTER", "VALOR  entro " + c++);
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_product_existencia_venta_new_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
            Log.d("SYNCADAPTER","VALOR  entroj "+co++);
        }
        File f = new File(Environment.getExternalStorageDirectory() + "/Pventa/" + mListAnswer.get(position).getImagen());
        Picasso.with(mContext).load(f).into(holder.profile_image);
        holder.price_product.setText("$ "+mListAnswer.get(position).getPrecio());
        holder.codigo_barras.setText(mListAnswer.get(position).getClave());

        return convertView;
    }

    class ViewHolder{

        @Bind(R.id.profile_image)CircleImageView profile_image;
        @Bind(R.id.price_product)TextView price_product;
        @Bind(R.id.codigo_barras)TextView codigo_barras;


        public ViewHolder(View v){
            ButterKnife.bind(this, v);
        }
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setIsCheck(boolean isCheck) {
        this.isCheck = isCheck;
    }

}