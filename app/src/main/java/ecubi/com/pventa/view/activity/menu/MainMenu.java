package ecubi.com.pventa.view.activity.menu;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.amulyakhare.textdrawable.TextDrawable;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.library.Alerts;
import ecubi.com.pventa.library.Utils;
import ecubi.com.pventa.view.activity.configuracion.Configuracion;
import ecubi.com.pventa.view.activity.cuentas_cobrar.CuentasCobrar;
import ecubi.com.pventa.view.activity.inventarios.Inventario;
import ecubi.com.pventa.view.activity.ventas.Ventas;


public class MainMenu extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu_portrait);

        if(Utils.detectIsMobil(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_main_menu_portrait);
        }else
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_main_menu_landscape);
        }
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        setTitle("Menu principal");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_back, menu);
        return true;
    }

    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Alerts.showDialogWithCallback(MainMenu.this, "¿Deseas salir de la aplicación?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick(R.id.cuenta_cobrar)
    public void cuenta_cobrar()
    {
        Intent inteMainMenu=new Intent(this, CuentasCobrar.class);
        startActivity(inteMainMenu);
    }

    @OnClick(R.id.inventario)
    public void inventario()
    {
        Intent inteMainMenu=new Intent(this, Inventario.class);
        startActivity(inteMainMenu);
    }


    @OnClick(R.id.ventas)
    public void ventas()
    {
        Intent inteMainMenu=new Intent(this, Ventas.class);
        startActivity(inteMainMenu);
    }

    @OnClick(R.id.configuracion)
    public void configuracion()
    {
        Intent inteMainMenu=new Intent(this, Configuracion.class);
        startActivity(inteMainMenu);
    }
}
