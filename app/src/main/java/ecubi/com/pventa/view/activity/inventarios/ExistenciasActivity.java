package ecubi.com.pventa.view.activity.inventarios;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;

import com.fourmob.datetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.library.Alerts;
import ecubi.com.pventa.model.sugar.entradasSalidas;
import ecubi.com.pventa.model.sugar.productData;
import ecubi.com.pventa.view.adapter.AdapterViewProductExistencia;
import ecubi.com.pventa.view.adapter.AdapterViewProductExistenciaReporte;
import ecubi.com.pventa.view.adapter.AdapterViewProductVendido;

public class ExistenciasActivity extends AppCompatActivity {


    @Bind(R.id.ListViewData)
    RecyclerView mListViewData;

    @Bind(R.id.periodo_uno)
    EditText periodo_uno_;

    @Bind(R.id.periodo_dos)
    EditText periodo_dos_;

    DatePickerDialog mDatePickerDialog;
    DatePickerDialog mDatePickerDialog2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existencias);


        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        setTitle("Existencia productos");

        setProductosVendidosAdapter(false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        mListViewData.setLayoutManager(linearLayoutManager);
        mListViewData.setHasFixedSize(true);
        periodo_uno_.setFocusable(false);
        periodo_dos_.setFocusable(false);




    }



    @OnClick(R.id.consultar)
    public void consultar()
    {
        if(periodo_uno_.getText().toString().trim().isEmpty() || periodo_dos_.getText().toString().trim().isEmpty())
        {
            Alerts.showToastMessage(this, "Necesitas seleccionar las fechas");
            return;
        }

        setProductosVendidosAdapter(true);
    }

    @OnClick(R.id.periodo_dos)
    public void periodo_dos()
    {
        mDatePickerDialog2.show(getSupportFragmentManager(),"datePicker");
    }

    @OnClick(R.id.periodo_uno)
    public void periodo_uno()
    {
        mDatePickerDialog.show(getSupportFragmentManager(),"datePicker");
    }

    public void setProductosVendidosAdapter(boolean filters)
    {

        List<productData> ListproductData=productData.listAll(productData.class);


        if(ListproductData.size()>0)
        {
            AdapterViewProductExistenciaReporte AdapterViewProductVendidotem=new AdapterViewProductExistenciaReporte(this,ListproductData);
            mListViewData.setAdapter(AdapterViewProductVendidotem);
        }else
        {
            mListViewData.setAdapter(null);
        }
    }

    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
