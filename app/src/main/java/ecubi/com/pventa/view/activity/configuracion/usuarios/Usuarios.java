package ecubi.com.pventa.view.activity.configuracion.usuarios;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.amulyakhare.textdrawable.TextDrawable;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.control.callback.user.CallbackUser;
import ecubi.com.pventa.library.Alerts;
import ecubi.com.pventa.library.Utils;
import ecubi.com.pventa.model.sugar.user;
import ecubi.com.pventa.view.adapter.AdapterUser;

public class Usuarios extends AppCompatActivity implements CallbackUser {

    @Bind(R.id.ImageT)
    ImageView ImageT;
    @Bind(R.id.user_name_activity)
    EditText mUserNameActivity;
    @Bind(R.id.email_activity)
    EditText mEmailActivity;
    @Bind(R.id.password_activity)
    EditText mPasswordActivity;
    @Bind(R.id.repet_password_activity)
    EditText mRepeatPasswordActivity;
    @Bind(R.id.chk_admin_user)
    CheckBox mChkAdminUser;

    @Bind(R.id.ListViewData)
    RecyclerView mListViewData;

    @Bind(R.id.save_user)
    Button mSaveUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuarios_portrait);

        if (Utils.detectIsMobil(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_usuarios_portrait);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_usuarios_landscape);
        }

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextDrawable drawable = TextDrawable.builder()
                .buildRound("1", Color.rgb(249, 172, 113));
        ImageT.setImageDrawable(drawable);


        setTitle("Usuarios");
        userItemUpdate=null;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        mListViewData.setLayoutManager(linearLayoutManager);
        mListViewData.setHasFixedSize(true);
        setInserData();
    }



    public void setInserData()
    {
        List<user> ListUser=user.listAll(user.class);
        if(ListUser.size()>0)
        {
            AdapterUser adapterUser=new AdapterUser(this,ListUser,this);
            mListViewData.setAdapter(adapterUser);
        }else
        {
            mListViewData.setAdapter(null);
        }
    }


    @OnClick(R.id.save_user)
    public void save_user()
    {
        String userName=mUserNameActivity.getText().toString().trim();
        String email=mEmailActivity.getText().toString().trim();
        String password=mPasswordActivity.getText().toString().trim();
        String passwordRepeat=mRepeatPasswordActivity.getText().toString().trim();

        if(userName.isEmpty() || email.isEmpty() || password.isEmpty() || passwordRepeat.isEmpty())
        {
            Alerts.showToastMessage(this,"Necesitas agregar todos los campos");
            return;
        }

        if(!password.equals(passwordRepeat))
        {
            Alerts.showToastMessage(this,"Los campos contraseña y repetir contraseña no coinciden.");
            return;
        }

        if(userItemUpdate==null) {
            user useritem = new user();
            useritem.setAdmin(mChkAdminUser.isChecked());
            useritem.setUserName(userName);
            useritem.setEmail(email);
            useritem.setPassword(password);
            useritem.save();
        }else {
            userItemUpdate.setAdmin(mChkAdminUser.isChecked());
            userItemUpdate.setUserName(userName);
            userItemUpdate.setEmail(email);
            userItemUpdate.setPassword(password);
            userItemUpdate=null;
        }


        mSaveUser.setText("Almacenar");
        mUserNameActivity.setText("");
        mEmailActivity.setText("");
        mPasswordActivity.setText("");
        mRepeatPasswordActivity.setText("");
        mChkAdminUser.setChecked(false);
        setInserData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_back, menu);
        return true;
    }

    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void ClickDelete(final user userItem) {
        Alerts.showDialogWithCallback(Usuarios.this, "¿Deseas eliminar el usuario "+userItem.getUserName()+"?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                userItem.delete();
                setInserData();
            }
        });
    }
    user userItemUpdate;
    @Override
    public void ClickEdit(final user userItem) {
        Alerts.showDialogWithCallback(Usuarios.this, "¿Deseas editar el usuario "+userItem.getUserName()+"?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                userItemUpdate=userItem;

                mUserNameActivity.setText(userItemUpdate.getUserName());
                mEmailActivity.setText(userItemUpdate.getEmail());
                mPasswordActivity.setText(userItemUpdate.getPassword());
                mRepeatPasswordActivity.setText(userItemUpdate.getPassword());
                mChkAdminUser.setChecked(userItemUpdate.getAdmin());

                mSaveUser.setText("Actualizar");
            }
        });
    }
}
