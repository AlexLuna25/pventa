package ecubi.com.pventa.view.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ecubi.com.appecubi.R;

/**
 * Created by pro on 23/03/17.
 */
public class AdapterCustomSpinnerCombobox extends ArrayAdapter<String> {

    private Context context1;
    private ArrayList<String> data;
    public Resources res;
    LayoutInflater inflater;

    public AdapterCustomSpinnerCombobox(Context context, ArrayList<String> objects) {
        super(context, R.layout.spinner_row, objects);

        context1 = context;
        data = objects;

        inflater = (LayoutInflater) context1
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        View row=null;

        switch (1)
        {
            case 0:
                row = inflater.inflate(R.layout.spinner_row_title, parent, false);
                TextView tvCategory = (TextView) row.findViewById(R.id.SpinnerRow);
                tvCategory.setText(data.get(position).toString().toUpperCase());
                return row;
            case 1:
                row = inflater.inflate(R.layout.spinner_row, parent, false);
                tvCategory = (TextView) row.findViewById(R.id.SpinnerRow);
                tvCategory.setText(data.get(position).toString());
                return row;
        }


        TextView tvCategory = (TextView) row.findViewById(R.id.SpinnerRow);
        tvCategory.setText(data.get(position).toString());

        return row;
    }
}
