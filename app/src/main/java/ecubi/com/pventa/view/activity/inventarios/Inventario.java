package ecubi.com.pventa.view.activity.inventarios;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.library.Utils;
import ecubi.com.pventa.view.activity.inventarios.ajuste.Ajuste;
import ecubi.com.pventa.view.activity.inventarios.articulos.Articulos;
import ecubi.com.pventa.view.activity.inventarios.grupos.Grupos;
import ecubi.com.pventa.view.activity.inventarios.marcas.Marcas;
import ecubi.com.pventa.view.activity.inventarios.unidades.Unidades;
import ecubi.com.pventa.view.activity.menu.MainMenu;

public class Inventario extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario_portrait);

        if(Utils.detectIsMobil(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_inventario_portrait);
        }else
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_inventario_landscape);
        }
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle("Menu de inventarios");
    }


    @OnClick(R.id.cardex)
    public void cardex()
    {
        Intent inteMainMenu=new Intent(this, Kardex.class);
        startActivity(inteMainMenu);
    }

    @OnClick(R.id.existencias)
    public void existencias()
    {
        Intent inteMainMenu=new Intent(this, ExistenciasActivity.class);
        startActivity(inteMainMenu);
    }

    @OnClick(R.id.show_marcas)
    public void show_marcas()
    {
        Intent inteMainMenu=new Intent(this, Marcas.class);
        startActivity(inteMainMenu);
    }

    @OnClick(R.id.show_unidades)
    public void show_unidades()
    {
        Intent inteMainMenu=new Intent(this, Unidades.class);
        startActivity(inteMainMenu);
    }

    @OnClick(R.id.show_grupos)
    public void show_grupos()
    {
        Intent inteMainMenu=new Intent(this, Grupos.class);
        startActivity(inteMainMenu);
    }

    @OnClick(R.id.show_articulos)
    public void show_articulos()
    {
        Intent inteMainMenu=new Intent(this, Articulos.class);
        startActivity(inteMainMenu);
    }


    @OnClick(R.id.ajustes)
    public void ajustes()
    {
        Intent inteMainMenu=new Intent(this, Ajuste.class);
        startActivity(inteMainMenu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_back, menu);
        return true;
    }

    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
