package ecubi.com.pventa.view.adapter;

import android.content.Context;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.control.callback.CallbackProduct;
import ecubi.com.pventa.model.sugar.entradasSalidas;
import ecubi.com.pventa.model.sugar.groupProduct;
import ecubi.com.pventa.model.sugar.productData;


public class AdapterViewProductVendido extends RecyclerView.Adapter<AdapterViewProductVendido.ListProduct>
{

    Context context;
    OnItemClickListener clickListener;
    OnItemLongClickListener longListener;




    List<entradasSalidas> ListproductData;
    //build to list with image, title and description
    public AdapterViewProductVendido(Context context, List<entradasSalidas> ListproductData)
    {
        this.ListproductData=ListproductData;
        this.context = context;
    }


    @Override
    public ListProduct onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_product_vendido_item, viewGroup, false);
        return new ListProduct(view);
    }


    public ListProduct ListProduct_;
    @Override
    public void onBindViewHolder(final ListProduct ListProduct_, final int i)
    {
        this.ListProduct_=ListProduct_;

        List<productData> ListproductData_=productData.find(productData.class,"id=?",ListproductData.get(i).getIdProducto()+"");

        if(ListproductData_.size()>0)
        {
            ListProduct_.mClaveSat.setText(ListproductData_.get(0).getClave());
            ListProduct_.mGrupo.setText(ListproductData_.get(0).getDescripcion());
            ListProduct_.mCodigoBarras.setText(ListproductData.get(i).getCantidad()+"");
        }else {
            ListProduct_.mClaveSat.setText("");
            ListProduct_.mGrupo.setText("");
            ListProduct_.mCodigoBarras.setText("");
        }


    }




    @Override
    public int getItemCount()
    {
        return ListproductData == null ? 0 : ListproductData.size();
    }

    public interface OnItemClickListener
    {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener itemClickListener)
    {
        this.clickListener = itemClickListener;
    }


    public interface OnItemLongClickListener
    {
        public void SetOnItemLongClickListener(View view, int position);
    }

    public void SetOnItemLongClickListener(final OnItemLongClickListener longListener)
    {
        this.longListener = longListener;
    }


    class ListProduct extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
    {

        TextView mClaveSat;
        TextView mGrupo;
        TextView mCodigoBarras;

        //de.hdodenhof.circleimageview.CircleImageView
        public ListProduct(View itemView)
        {
            super(itemView);
            //Initialize data
            mClaveSat =(TextView) itemView.findViewById(R.id.clave_sat);
            mGrupo=(TextView) itemView.findViewById(R.id.grupo);
            mCodigoBarras=(TextView) itemView.findViewById(R.id.codigo_barras);


        }

        @Override
        public void onClick(View v)
        {
            clickListener.onItemClick(v, getPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            try{
                longListener.SetOnItemLongClickListener(v, getPosition());
            }catch (Exception g)
            {

            }

            return false;
        }
    }

}


