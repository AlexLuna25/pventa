package ecubi.com.pventa.view.adapter;

import android.content.Context;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.control.callback.CallbackProduct;
import ecubi.com.pventa.model.sugar.ListproductData;
import ecubi.com.pventa.model.sugar.groupProduct;
import ecubi.com.pventa.model.sugar.productData;


public class AdapterViewProductVenta extends RecyclerView.Adapter<AdapterViewProductVenta.ListProduct>
{

    Context context;
    OnItemClickListener clickListener;
    OnItemLongClickListener longListener;

    CallbackProduct callbackProduct;



    List<ecubi.com.pventa.model.sugar.ListproductData> ListproductData;
    //build to list with image, title and description
    public AdapterViewProductVenta(Context context, List<ListproductData> ListproductData, CallbackProduct callbackProduct)
    {
        this.ListproductData=ListproductData;
        this.context = context;
        this.callbackProduct=callbackProduct;
    }


    @Override
    public ListProduct onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_product_existencia_venta_item, viewGroup, false);
        return new ListProduct(view);
    }


    public ListProduct ListProduct_;
    @Override
    public void onBindViewHolder(final ListProduct ListProduct_, final int i)
    {
        this.ListProduct_=ListProduct_;
        File f = new File(Environment.getExternalStorageDirectory() + "/Pventa/" + ListproductData.get(i).getProducto().getImagen());
        Picasso.with(context).load(f).into(ListProduct_.ImageProfileState);

        ListProduct_.mClaveSat.setText(ListproductData.get(i).getProducto().getExistencia() + "");
        ListProduct_.mCodigoBarras.setText(ListproductData.get(i).getCantidad()+"");

        ListProduct_.price_product.setText(ListproductData.get(i).getProducto().getClave());


        String precio_unitario=(ListproductData.get(i).getCantidad()*Double.parseDouble(ListproductData.get(i).getProducto().getPrecio()))+"";
        ListProduct_.precio_total.setText(precio_unitario);
        ListProduct_.mGrupo.setText(ListproductData.get(i).getProducto().getPrecio());


        ListProduct_.mEditUserGroup.setVisibility(View.GONE);
        ListProduct_.mDeleteUserGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbackProduct.ClickDelete(ListproductData.get(i).getProducto());
            }
        });

        ListProduct_.mEditUserGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbackProduct.ClickEdit(ListproductData.get(i).getProducto());
            }
        });
    }




    @Override
    public int getItemCount()
    {
        return ListproductData == null ? 0 : ListproductData.size();
    }

    public interface OnItemClickListener
    {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener itemClickListener)
    {
        this.clickListener = itemClickListener;
    }


    public interface OnItemLongClickListener
    {
        public void SetOnItemLongClickListener(View view, int position);
    }

    public void SetOnItemLongClickListener(final OnItemLongClickListener longListener)
    {
        this.longListener = longListener;
    }


    class ListProduct extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
    {

        CircleImageView ImageProfileState;
        TextView mClaveSat;
        TextView mGrupo;
        TextView mCodigoBarras;
        ImageButton mDeleteUserGroup;
        ImageButton mEditUserGroup;
        TextView price_product;
        TextView precio_total;

        //de.hdodenhof.circleimageview.CircleImageView
        public ListProduct(View itemView)
        {
            super(itemView);
            //Initialize data
            ImageProfileState = (CircleImageView) itemView.findViewById(R.id.profile_image);
            mClaveSat =(TextView) itemView.findViewById(R.id.clave_sat);
            mGrupo=(TextView) itemView.findViewById(R.id.grupo);
            mCodigoBarras=(TextView) itemView.findViewById(R.id.codigo_barras);
            mDeleteUserGroup=(ImageButton) itemView.findViewById(R.id.delete_user_group);
            mEditUserGroup=(ImageButton) itemView.findViewById(R.id.edit_user_group);
            price_product=(TextView) itemView.findViewById(R.id.price_product);
            precio_total=(TextView) itemView.findViewById(R.id.precio_total);

        }

        @Override
        public void onClick(View v)
        {
            clickListener.onItemClick(v, getPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            try{
                longListener.SetOnItemLongClickListener(v, getPosition());
            }catch (Exception g)
            {

            }

            return false;
        }
    }

}


