package ecubi.com.pventa.view.activity.ventas.ventas;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnTextChanged;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.control.callback.CallbackProduct;
import ecubi.com.pventa.library.Alerts;
import ecubi.com.pventa.library.Utils;
import ecubi.com.pventa.model.sugar.CorteCaja;
import ecubi.com.pventa.model.sugar.ListproductData;
import ecubi.com.pventa.model.sugar.entradasSalidas;
import ecubi.com.pventa.model.sugar.productData;
import ecubi.com.pventa.model.sugar.ventaProductos;
import ecubi.com.pventa.model.sugar.ventaRealizada;
import ecubi.com.pventa.view.adapter.AdapterVentaProductoData;
import ecubi.com.pventa.view.adapter.AdapterViewProductVenta;


public class VentasActivity extends AppCompatActivity implements CallbackProduct {

    @Bind(R.id.total_producto)
    EditText total_producto;

    @Bind(R.id.subtotal_producto)
    EditText subtotal_producto;


    @Bind(R.id.ticket_)
    TextView ticket_;



    @Bind(R.id.total_piezas)
    EditText total_piezas;

    @Bind(R.id.codigo_barras_producto)
    EditText codigo_barras_producto;

    @Bind(R.id.item_data_catalog_list_results)
    ListView item_data_catalog_list_results;

    @Bind(R.id.cantidad_producto)
    EditText cantidad_producto;

    @Bind(R.id.ListViewData)
    RecyclerView mListViewData;

    productData productDataItemSelect;

    List<ListproductData> ListproductDataItemSelect;

    boolean search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventas_ventas_landscape);

        if(Utils.detectIsMobil(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_ventas_ventas_landscape);
        }else
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_ventas_ventas_landscape);
        }
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        productDataItemSelect=null;
        ListproductDataItemSelect=null;

        setTitle("Venta de productos");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        mListViewData.setLayoutManager(linearLayoutManager);
        mListViewData.setHasFixedSize(true);
        search=true;

        ticket_.setText("Ticket: "+(ventaRealizada.count(ventaRealizada.class) + 1) );

    }

    List<productData> ListproductDataSelect;

    @OnTextChanged(R.id.codigo_barras_producto)
    public void codigo_barras_producto()
    {
        if(!search)
        {
            return;
        }
        List<productData> ListproductData=productData.listAll(productData.class);
        if(ListproductData.size()>0)
        {
            String nameProduct=codigo_barras_producto.getText().toString().trim();
            if(nameProduct.isEmpty())
            {
                item_data_catalog_list_results.setVisibility(View.GONE);
                return;
            }
            ListproductDataSelect=new ArrayList<>();
            for(productData productDataItem:ListproductData)
            {
                if(productDataItem.getCodigoBarras().toUpperCase().contains(nameProduct.toUpperCase()) || productDataItem.getClave().toUpperCase().contains(nameProduct.toUpperCase()) || productDataItem.getDescripcion().toUpperCase().contains(nameProduct.toUpperCase()))
                {
                    ListproductDataSelect.add(productDataItem);
                    break;
                }
            }
            AdapterVentaProductoData AdapterVentaProductoDataItem=new AdapterVentaProductoData(this,R.layout.view_product_existencia_venta_new_item,ListproductDataSelect);

            item_data_catalog_list_results.setAdapter(AdapterVentaProductoDataItem);

            item_data_catalog_list_results.setVisibility(View.VISIBLE);

        }
    }

    public void setInsertdata()
    {
        if(ListproductDataItemSelect.size()>0)
        {
            AdapterViewProductVenta adap=new AdapterViewProductVenta(this,ListproductDataItemSelect,this);
            mListViewData.setAdapter(adap);
            int cantidad=0;
            double precio=0;
            for(ListproductData ListproductDataItem:ListproductDataItemSelect)
            {
                cantidad+=ListproductDataItem.getCantidad();
                precio+= (ListproductDataItem.getCantidad()*Double.parseDouble(ListproductDataItem.getProducto().getPrecio()));
            }
            total_piezas.setText(cantidad+"");
            total_producto.setText("$ "+precio);
            subtotal_producto.setText("$ "+precio);
        }else
        {

            total_piezas.setText("0");
            total_producto.setText("$ 0");
            subtotal_producto.setText("$ 0");
            mListViewData.setAdapter(null);
        }

    }


    @OnClick(R.id.save_product)
    public void save_product()
    {
        if(ListproductDataItemSelect==null )
        {
            Alerts.showToastMessage(this,"Para realizar una venta necesitas seleccionar los productos");
            return;
        }
        if(ListproductDataItemSelect.size()==0)
        {
            Alerts.showToastMessage(this, "Para realizar una venta necesitas seleccionar los productos");
            return;
        }


        Alerts.showDialogWithCallback(VentasActivity.this, "¿Deseas realizar la venta?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                long idCorte=0;
                /*antes de realizar la venta valida si ya esta el corte del dia de hoy*/
                List<CorteCaja> ListCorteCaja=CorteCaja.find(CorteCaja.class,"fecha=?", Utils.getCurrentTimeCityDate2() + "");
                if(ListCorteCaja.size()>0)
                {
                    idCorte=ListCorteCaja.get(0).getId();
                }else
                {/*no existe entonces creala*/
                    CorteCaja cort=new CorteCaja();
                    cort.setFecha(Utils.getCurrentTimeCityDate2());
                    idCorte=cort.save();
                }

        /*ahora crea la venta*/
                ventaRealizada ventaRealizadaNew=new ventaRealizada();
                String ticket=(ventaRealizada.count(ventaRealizada.class) + 1) + "";
                ventaRealizadaNew.setTicketVenta(ticket);
                ventaRealizadaNew.setFechaVenta(Utils.getCurrentTimeCityDate());
                ventaRealizadaNew.setCantidadProductos(total_piezas.getText().toString());
                ventaRealizadaNew.setPrecioProductos(total_producto.getText().toString());
                ventaRealizadaNew.setCancelada(false);
                ventaRealizadaNew.setCorteCaja(true);
                ventaRealizadaNew.setIdCorteCaja(idCorte+"");
                long idVenta=ventaRealizadaNew.save();

                for(ListproductData ListproductDataItem:ListproductDataItemSelect)
                {
                    ventaProductos ventaProductosItem=new ventaProductos();
                    ventaProductosItem.setTicketVenta(ticket);
                    ventaProductosItem.setCantidadProductos(ListproductDataItem.getCantidad() + "");
                    ventaProductosItem.setPrecioProductos(ListproductDataItem.getCantidad() * Double.parseDouble(ListproductDataItem.getProducto().getPrecio())+"");
                    ventaProductosItem.setPrecioUnicoProducto(ListproductDataItem.getProducto().getPrecio() + "");
                    ventaProductosItem.save();
            /*por cada producto se debe de */


                    entradasSalidas entradasSalidasNew=new entradasSalidas();
                    entradasSalidasNew.setIdProducto(ListproductDataItem.getProducto().getId() + "");
                    entradasSalidasNew.setCantidad(ListproductDataItem.getCantidad());
                    entradasSalidasNew.setConcepto("Salida");
                    entradasSalidasNew.setFecha(Utils.getCurrentTimeCityDate());
                    entradasSalidasNew.setPrecio(ListproductDataItem.getProducto().getPrecio() + "");
                    entradasSalidasNew.save();

                    int cantidaActual=ListproductDataItem.getProducto().getExistencia()-ListproductDataItem.getCantidad();
                    productData productDataUpdate=ListproductDataItem.getProducto();
                    productDataUpdate.setExistencia((productDataUpdate.getExistencia()-ListproductDataItem.getCantidad()));
                    productDataUpdate.save();
                }
                Alerts.showToastMessage(getApplication(), "La venta se realizo correctamente");
                Intent ventaInten=new Intent(getApplication(),VentasActivity.class);
                startActivity(ventaInten);
                finish();
            }
        });



    }


    @OnClick(R.id.cancelar_venta)
    public void cancelar_venta()
    {
        AlertDialog.Builder dialoge= new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View ProgressView =  inflater.inflate(R.layout.dialog_cancel_venta, null);
        final EditText textRef= (EditText) ProgressView.findViewById(R.id.editref);
        dialoge.setView(ProgressView);
        dialoge.setTitle("Cancelar venta");
        dialoge.setMessage("¿Deseas cancelar una venta?");
        dialoge.setNeutralButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String folio=textRef.getText().toString();
                final List<ventaRealizada> ListventaRealizada=ventaRealizada.find(ventaRealizada.class,"ticket_Venta=?",folio);
                if(ListventaRealizada.size()>0)
                {
                    Alerts.showDialogWithCallback(VentasActivity.this, "¿Deseas cancelar la venta?", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ventaRealizada itemventaRealizada=ListventaRealizada.get(0);
                            itemventaRealizada.setCancelada(true);
                            itemventaRealizada.save();
                            Alerts.showToastMessage(getApplication(), "La venta se cancelo correctamente");
                        }
                    });
                }else
                {
                    Alerts.showToastMessage(getApplication(),"El Ticket de venta ingresado no existe");
                }

            }
        });

        dialoge.show();
    }

    @OnClick(R.id.add_product)
    public void add_product_metodo()
    {
        String CantidadaProduct=cantidad_producto.getText().toString().trim();
        if(CantidadaProduct.isEmpty())
        {
            Alerts.showToastMessage(this,"Necesitas agregar la cantidad de producto");
            return;
        }

        int cantidadNumber=0;
        try{
            cantidadNumber=Integer.parseInt(CantidadaProduct);
        }catch (Exception g)
        {
            Alerts.showToastMessage(this,"Necesitas agregar la cantidad de producto");
            return;
        }

        if(productDataItemSelect==null)
        {
            Alerts.showToastMessage(this,"Necesitas seleccionar el producto");
            return;
        }

        /*ahora empieza a agregar la lista de productos*/
        if(ListproductDataItemSelect==null)
        {
            ListproductDataItemSelect=new ArrayList<>();
        }

        /*valida la existencia si existela cantidad que escribio el usuario*/
        ListproductData listproductData=new ListproductData();
        listproductData.setCantidad(cantidadNumber);
        listproductData.setProducto(productDataItemSelect);

        boolean insert=false;
        for(ListproductData listproductData1:ListproductDataItemSelect)
        {
            if(listproductData1.getProducto().getId()==productDataItemSelect.getId())
            {
                /*valida la existencia*/
                int cantidadExistencia=listproductData1.getCantidad()+cantidadNumber;
                if(cantidadExistencia>productDataItemSelect.getExistencia())
                {
                    Alerts.showToastMessage(this,"La cantidad que deseas agregar es mayor a la cantidad existente");
                    return;
                }else {
                    listproductData1.setCantidad(cantidadExistencia);
                    insert=true;
                }

            }
        }
        if(!insert) {
            /*solo valida la existencia*/
            if(cantidadNumber>productDataItemSelect.getExistencia())
            {
                Alerts.showToastMessage(this,"La cantidad que deseas agregar es mayor a la cantidad existente");
                return;
            }else {
                ListproductDataItemSelect.add(listproductData);
            }
        }

        productDataItemSelect=null;
        codigo_barras_producto.setText("");
        cantidad_producto.setText("1");
        setInsertdata();
    }

    @OnItemClick(R.id.item_data_catalog_list_results)
    public void item_data_catalog_list_results_(AdapterView<?> parent, View view, int position, long id)
    {
        search=false;
        productDataItemSelect= ListproductDataSelect.get(position);
        codigo_barras_producto.setText(productDataItemSelect.getClave());
        item_data_catalog_list_results.setAdapter(null);
        item_data_catalog_list_results.setVisibility(View.GONE);
        search=true;
    }


    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void ClickDelete(final productData groupProductItem) {
        Alerts.showDialogWithCallback(VentasActivity.this, "¿Deseas eliminar el producto "+groupProductItem.getClave()+" de tus ventas?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                int position=0;
                for(ListproductData listproductData1:ListproductDataItemSelect) {
                    if (listproductData1.getProducto().getId() == groupProductItem.getId()) {
                        ListproductDataItemSelect.remove(position);
                        break;
                    }
                    position++;
                }
                setInsertdata();
            }
        });
    }

    @Override
    public void ClickEdit(productData groupProductItem) {

    }
}
