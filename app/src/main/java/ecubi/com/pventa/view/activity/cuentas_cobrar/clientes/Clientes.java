package ecubi.com.pventa.view.activity.cuentas_cobrar.clientes;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.library.Utils;

public class Clientes extends AppCompatActivity {

    @Bind(R.id.ImageT)ImageView ImageT;
    @Bind(R.id.ImageTdomicilio)ImageView ImageTdomicilio;
    @Bind(R.id.content_general) LinearLayout content_general;
    @Bind(R.id.content_domicilio) LinearLayout content_domicilio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes_portrait);

        if(Utils.detectIsMobil(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_clientes_portrait);
        }else
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_clientes_landscape);
        }

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextDrawable drawable = TextDrawable.builder()
                .buildRound("1", Color.rgb(249, 172, 113));
        ImageT.setImageDrawable(drawable);
        TextDrawable drawableDomicilio = TextDrawable.builder()
                .buildRound("2", Color.rgb(249, 172, 113));
        ImageTdomicilio.setImageDrawable(drawableDomicilio);

        setTitle("Cliente");
    }


    @OnClick(R.id.nextTabGen)
    public void nextTabGen()
    {
        content_general.setVisibility(View.GONE);
        content_domicilio.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.prevTabDomicilio)
    public void prevTabDomicilio()
    {
        content_general.setVisibility(View.VISIBLE);
        content_domicilio.setVisibility(View.GONE);
    }

    @OnClick(R.id.nextTabDomicilio)
    public void nextTabDomicilio()
    {

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_back, menu);
        return true;
    }

    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
