package ecubi.com.pventa.view.adapter;

import android.content.Context;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.control.callback.CallbackProduct;
import ecubi.com.pventa.model.sugar.ListproductData;
import ecubi.com.pventa.model.sugar.productData;


public class AdapterViewProductVentaNew extends RecyclerView.Adapter<AdapterViewProductVentaNew.ListProduct>
{

    Context context;
    OnItemClickListener clickListener;
    OnItemLongClickListener longListener;

    CallbackProduct callbackProduct;



    List<productData> ListproductData;
    //build to list with image, title and description
    public AdapterViewProductVentaNew(Context context, List<productData> ListproductData)
    {
        this.ListproductData=ListproductData;
        this.context = context;
        this.callbackProduct=callbackProduct;
    }


    @Override
    public ListProduct onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_product_existencia_venta_new_item, viewGroup, false);
        return new ListProduct(view);
    }


    public ListProduct ListProduct_;
    @Override
    public void onBindViewHolder(final ListProduct ListProduct_, final int i)
    {
        this.ListProduct_=ListProduct_;
        File f = new File(Environment.getExternalStorageDirectory() + "/Pventa/" + ListproductData.get(i).getImagen());
        Picasso.with(context).load(f).into(ListProduct_.ImageProfileState);

        ListProduct_.mCodigoBarras.setText(ListproductData.get(i).getClave() + "");


        ListProduct_.precio_total.setText(ListproductData.get(i).getPrecio()+"");




    }




    @Override
    public int getItemCount()
    {
        return ListproductData == null ? 0 : ListproductData.size();
    }

    public interface OnItemClickListener
    {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener itemClickListener)
    {
        this.clickListener = itemClickListener;
    }


    public interface OnItemLongClickListener
    {
        public void SetOnItemLongClickListener(View view, int position);
    }

    public void SetOnItemLongClickListener(final OnItemLongClickListener longListener)
    {
        this.longListener = longListener;
    }


    class ListProduct extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
    {

        CircleImageView ImageProfileState;
        TextView precio_total;
        TextView mCodigoBarras;




        //de.hdodenhof.circleimageview.CircleImageView
        public ListProduct(View itemView)
        {
            super(itemView);
            //Initialize data
            ImageProfileState = (CircleImageView) itemView.findViewById(R.id.profile_image);
            mCodigoBarras=(TextView) itemView.findViewById(R.id.codigo_barras);
            precio_total=(TextView) itemView.findViewById(R.id.precio_total);

        }

        @Override
        public void onClick(View v)
        {
            clickListener.onItemClick(v, getPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            try{
                longListener.SetOnItemLongClickListener(v, getPosition());
            }catch (Exception g)
            {

            }

            return false;
        }
    }

}


