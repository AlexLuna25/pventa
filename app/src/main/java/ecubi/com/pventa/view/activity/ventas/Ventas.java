package ecubi.com.pventa.view.activity.ventas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.library.Utils;
import ecubi.com.pventa.view.activity.inventarios.ajuste.Ajuste;
import ecubi.com.pventa.view.activity.ventas.ventas.VentasActivity;

public class Ventas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventas_portrait);

        if(Utils.detectIsMobil(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_ventas_portrait);
        }else
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_ventas_landscape);
        }
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle("Menu de ventas");
    }

    @OnClick(R.id.corte_caja)
    public void corte_caja()
    {
        Intent inteMainMenu=new Intent(this, CorteCajaActivity.class);
        startActivity(inteMainMenu);
    }

    @OnClick(R.id.ventas_user)
    public void ventas_user()
    {
        Intent inteMainMenu=new Intent(this, VentasActivity.class);
        startActivity(inteMainMenu);
    }

    @OnClick(R.id.productos_vendidos)
    public void productos_vendidos()
    {
        Intent inteMainMenu=new Intent(this, ProductoVendidos.class);
        startActivity(inteMainMenu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_back, menu);
        return true;
    }

    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
