package ecubi.com.pventa.view.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ecubi.com.pventa.control.callback.groupProduct.CallbackGroup;
import ecubi.com.pventa.model.sugar.groupProduct;
import ecubi.com.appecubi.R;
/**
 * Created by Alejandro
 */
public class AdapterGroup extends RecyclerView.Adapter<AdapterGroup.ListRoute> {


    OnItemClickListener clickListener;
    List<groupProduct> ListModel;
    Context context;
    CallbackGroup callbackGroup;
    public AdapterGroup(Context context, List<groupProduct> ListModel,CallbackGroup callbackGroup)
    {
        this.callbackGroup=callbackGroup;
        this.context=context;
        this.ListModel=ListModel;
    }

    @Override
    public ListRoute onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_group, viewGroup, false);
        return new ListRoute(view);
    }


    public ListRoute ListProduct_;
    @Override
    public void onBindViewHolder(final ListRoute ListRoute_, final int i)
    {
        ListRoute_.mTitleNameUserGroup.setText(ListModel.get(i).getName()+"");

        ListRoute_.mDeleteUserGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbackGroup.ClickDelete(ListModel.get(i));
            }
        });

        ListRoute_.mEditUserGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbackGroup.ClickEdit(ListModel.get(i));
            }
        });
    }






    @Override
    public int getItemCount()
    {
        return ListModel == null ? 0 : ListModel.size();
    }

    public interface OnItemClickListener
    {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener itemClickListener)
    {
        this.clickListener = itemClickListener;
    }


    public interface OnItemLongClickListener
    {
        public void SetOnItemLongClickListener(View view, int position);
    }




    class ListRoute extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView mTitleNameUserGroup;
        ImageButton mDeleteUserGroup;
        ImageButton mEditUserGroup;


        public ListRoute(View itemView)
        {
            super(itemView);
            mTitleNameUserGroup=(TextView) itemView.findViewById(R.id.title_name_user_group);
            mDeleteUserGroup=(ImageButton) itemView.findViewById(R.id.delete_user_group);
            mEditUserGroup=(ImageButton) itemView.findViewById(R.id.edit_user_group);


            //itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            clickListener.onItemClick(v, getPosition());
        }

    }

}


