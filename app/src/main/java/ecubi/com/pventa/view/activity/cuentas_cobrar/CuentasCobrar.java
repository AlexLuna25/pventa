package ecubi.com.pventa.view.activity.cuentas_cobrar;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.library.Utils;
import ecubi.com.pventa.view.activity.cuentas_cobrar.clientes.Clientes;
import ecubi.com.pventa.view.activity.cuentas_cobrar.clientes.TipoClientes;
import ecubi.com.pventa.view.activity.ventas.Ventas;

public class CuentasCobrar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuentas_cobrar_portrait);

        if(Utils.detectIsMobil(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_cuentas_cobrar_portrait);
        }else
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_cuentas_cobrar_landscape);
        }
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle("Cuentas por cobrar");
    }


    @OnClick(R.id.show_client)
    public void show_client()
    {
        Intent inteMainMenu=new Intent(this, Clientes.class);
        startActivity(inteMainMenu);
    }

    @OnClick(R.id.show_type_client)
    public void show_type_client()
    {
        Intent inteMainMenu=new Intent(this, TipoClientes.class);
        startActivity(inteMainMenu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_back, menu);
        return true;
    }

    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
