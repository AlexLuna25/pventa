package ecubi.com.pventa.view.activity.inventarios.articulos;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.amulyakhare.textdrawable.TextDrawable;
import com.google.zxing.integration.android.IntentIntegrator;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.background.sessions.GlobalSession;
import ecubi.com.pventa.control.callback.CallbackProduct;
import ecubi.com.pventa.library.Alerts;
import ecubi.com.pventa.library.CameraUtils;
import ecubi.com.pventa.library.Utils;
import ecubi.com.pventa.library.scan.ScanCodeBarActivity;
import ecubi.com.pventa.model.sugar.groupProduct;
import ecubi.com.pventa.model.sugar.productData;
import ecubi.com.pventa.view.adapter.AdapterCustomSpinnerCombobox;
import ecubi.com.pventa.view.adapter.AdapterViewProduct;

public class Articulos extends AppCompatActivity implements CallbackProduct {

    @Bind(R.id.ImageT)ImageView ImageT;
    @Bind(R.id.save_product)
    Button mSaveProduct;

    @Bind(R.id.ListViewData)
    RecyclerView mListViewData;

    @Bind(R.id.clave_product)
    EditText mClaveProduct;
    @Bind(R.id.descripcion_producto)
    EditText mDescripcionProducto;

    @Bind(R.id.codigo_barras_producto)
    EditText mCodigoBarrasProducto;

    @Bind(R.id.precio_productos)
    EditText mPrecioProducto;

    @Bind(R.id.calve_sat_productos)
    EditText mClaveSatProducto;

    @Bind(R.id.group)
    Spinner group;

    @Bind(R.id.product_image) ImageView product_image;
    public  boolean selectedImageUri1;
    private static final int SELECT_IMAGE_PRODUCT = 100;

    public static final Collection<String> CODE_BAR_INTERMEX_TYPES = listCodeBar(new String[]{"EAN_13", "UPC_A"});

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articulos_portrait);

        if(Utils.detectIsMobil(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_articulos_portrait);
        }else
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_articulos_landscape);
        }

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextDrawable drawable = TextDrawable.builder()
                .buildRound("1", Color.rgb(249, 172, 113));
        ImageT.setImageDrawable(drawable);

        setTitle("Productos");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        mListViewData.setLayoutManager(linearLayoutManager);
        mListViewData.setHasFixedSize(true);



        groupProductItemUpdate=null;

        setInserData();
    }

    List<groupProduct> ListgroupProduct;
    public void setInserData()
    {
        ListgroupProduct=groupProduct.listAll(groupProduct.class);

        if(ListgroupProduct.size()>0)
        {
            ArrayList<String> data=new ArrayList<>();
            for(groupProduct groupProductItem:ListgroupProduct)
            {
                data.add(groupProductItem.getName());
            }
            AdapterCustomSpinnerCombobox spinAdapterOperation = new AdapterCustomSpinnerCombobox(this,data);
            spinAdapterOperation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            group.setAdapter(spinAdapterOperation);


            List<productData> ListproductData=productData.listAll(productData.class);
            if(ListproductData.size()>0)
            {
                AdapterViewProduct adpaterViweProduct=new AdapterViewProduct(this,ListproductData,this);
                mListViewData.setAdapter(adpaterViweProduct);
            }else
            {
                mListViewData.setAdapter(null);
            }

        }else
        {
            Alerts.showToastMessage(this,"Para dar de alta un producto necesitas dar de alta un grupo");
            finish();
            return;
        }
    }

    /*cambia la imagene del producto*/
    @OnClick(R.id.change_image)
    public void ChangeImageProduct()
    {
        CameraUtils.takePhoto(this);
    }


    @OnClick(R.id.save_product)
    public void save_product()
    {
        String calveProduct=mClaveProduct.getText().toString().trim();
        String descripcion=mDescripcionProducto.getText().toString().trim();
        String codigoBarras=mCodigoBarrasProducto.getText().toString().trim();
        String precio=mPrecioProducto.getText().toString().trim();
        String claveSat=mClaveSatProducto.getText().toString().trim();


        if(!selectedImageUri1)
        {
            Alerts.showToastMessage(this,"Necesitas seleccionar imagen del producto");
            return;
        }

        if(calveProduct.isEmpty() || descripcion.isEmpty() || codigoBarras.isEmpty() || precio.isEmpty() || claveSat.isEmpty())
        {
            Alerts.showToastMessage(this,"Necesitas llenar todos los campos del producto");
            return;
        }


        if(groupProductItemUpdate==null) {
        /*antes de realizar alguna accion  copia la imagen a la carpeta*/
            File storagePath = new File(Environment.getExternalStorageDirectory(), "Pventa");
            boolean created = storagePath.mkdirs();
            String nameImage = UUID.randomUUID().toString();
            try {
                copy(CameraUtils.lastPhotoPath, (new File(Environment.getExternalStorageDirectory() + "/Pventa/" + nameImage + ".jpg")));
            } catch (IOException e) {
                String error = "";
            }

            productData productDataItem = new productData();
            productDataItem.setImagen(nameImage + ".jpg");
            productDataItem.setClave(calveProduct);
            productDataItem.setDescripcion(descripcion);
            productDataItem.setCodigoBarras(codigoBarras);
            productDataItem.setPrecio(precio);
            productDataItem.setIdGrupo(ListgroupProduct.get(group.getSelectedItemPosition()).getId() + "");
            productDataItem.setClaveSat(claveSat);
            productDataItem.setFechaUltimaCompra("");
            productDataItem.setFechaUltimaVenta("");
            productDataItem.setExistencia(0);
            productDataItem.save();
            Alerts.showToastMessage(this, "Producto almacenado correctamente");


        }



        mClaveProduct.setText("");
        mDescripcionProducto.setText("");
        mCodigoBarrasProducto.setText("");
        mPrecioProducto.setText("");
        mClaveSatProducto.setText("");
        selectedImageUri1=false;
        Picasso.with(this).load(R.drawable.default_user).into(product_image);

        mSaveProduct.setText("Almacenar");
        setInserData();

    }


    @OnClick(R.id.scan_code_product)
    public void scan_code_product()
    {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setBarcodeImageEnabled(true);
        integrator.setCaptureActivity(ScanCodeBarActivity.class);
        integrator.setDesiredBarcodeFormats(CODE_BAR_INTERMEX_TYPES);
        integrator.initiateScan();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_back, menu);
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String codeBar=(new GlobalSession(this)).GetDataCodeBar();
        if(codeBar.length()>0)
        {
            mCodigoBarrasProducto.setText(codeBar);
            (new GlobalSession(this)).SetDataCodeBar("");
        }
        if(resultCode==RESULT_OK){
            if (requestCode == CameraUtils.REQUEST_TAKE_PHOTO) {
                File mSaveBit=CameraUtils.lastPhotoPath;
                String filePath = mSaveBit.getPath();
                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                product_image.setImageBitmap(bitmap);
                selectedImageUri1=true;
            }
        }
    }

    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private static List<String> listCodeBar(String... values) {
        return Collections.unmodifiableList(Arrays.asList(values));
    }

    private String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public void copy(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }

    @Override
    public void ClickDelete(final productData groupProductItem) {
        Alerts.showDialogWithCallback(Articulos.this, "¿Deseas eliminar el producto "+groupProductItem.getClave()+"?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                groupProductItem.delete();
                setInserData();
            }
        });
    }
    productData groupProductItemUpdate;
    @Override
    public void ClickEdit(final productData groupProductItem) {
        Alerts.showDialogWithCallback(Articulos.this, "¿Deseas editar el producto "+groupProductItem.getClave()+"?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                groupProductItemUpdate=groupProductItem;
                mSaveProduct.setText("Modificar");

                mClaveProduct.setText(groupProductItem.getClave());
                mDescripcionProducto.setText(groupProductItem.getDescripcion());
                mCodigoBarrasProducto.setText(groupProductItem.getCodigoBarras());
                mPrecioProducto.setText(groupProductItem.getPrecio());
                mClaveSatProducto.setText(groupProductItem.getClaveSat());
                selectedImageUri1=false;
                File f = new File(Environment.getExternalStorageDirectory() + "/Pventa/" + groupProductItem.getImagen());
                Picasso.with(getApplication()).load(f).into(product_image);

            }
        });
    }
}
