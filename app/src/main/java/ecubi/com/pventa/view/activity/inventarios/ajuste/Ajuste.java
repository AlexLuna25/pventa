package ecubi.com.pventa.view.activity.inventarios.ajuste;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.control.callback.CallbackProduct;
import ecubi.com.pventa.library.Alerts;
import ecubi.com.pventa.library.Utils;
import ecubi.com.pventa.model.sugar.entradasSalidas;
import ecubi.com.pventa.model.sugar.productData;
import ecubi.com.pventa.view.adapter.AdapterViewProduct;
import ecubi.com.pventa.view.adapter.AdapterViewProductExistencia;

public class Ajuste extends AppCompatActivity implements CallbackProduct {


    @Bind(R.id.nombre_producto_select)
    EditText mNombreProductoSelect;
    @Bind(R.id.save_product)
    Button mSaveProduct;
    @Bind(R.id.ImageT)ImageView ImageT;

    @Bind(R.id.existencia_now) EditText mExistenciaNow;
    @Bind(R.id.existencia_new) EditText mExistenciaNew;


    @Bind(R.id.ListViewData)
    RecyclerView mListViewData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajuste_landscape);


        if(Utils.detectIsMobil(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_ajuste_landscape);
        }else
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_ajuste_landscape);
        }

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        TextDrawable drawable = TextDrawable.builder()
                .buildRound("1", Color.rgb(249, 172, 113));
        ImageT.setImageDrawable(drawable);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        mListViewData.setLayoutManager(linearLayoutManager);
        mListViewData.setHasFixedSize(true);
        mSaveProduct.setEnabled(false);
        setInserData();
    }


    public void setInserData()
    {
        List<productData> ListproductData=productData.listAll(productData.class);
        if(ListproductData.size()>0)
        {
            AdapterViewProductExistencia adpaterViweProduct=new AdapterViewProductExistencia(this,ListproductData,this);
            mListViewData.setAdapter(adpaterViweProduct);
        }else
        {
            mListViewData.setAdapter(null);
        }
    }


    @OnClick(R.id.save_product)
    public void save_product()
    {
        if(groupProductItemUpdate==null)
        {
            return;
        }
        String cantidadNew=mExistenciaNew.getText().toString();
        if(cantidadNew.isEmpty())
        {
            Alerts.showToastMessage(this,"Necesitas seleccionar la nueva cantidad");
            return;
        }

        int newCantidad;
        try{
            newCantidad=Integer.parseInt(cantidadNew);
            if(newCantidad<0)
            {
                Alerts.showToastMessage(this,"La cantidad del producto debe ser mayor o igual cero");
                return;
            }
        }catch (Exception g)
        {
            Alerts.showToastMessage(this,"Cantidad escrita no es correcta");
            return;
        }


        /*ahora valida si fue entrada o salida*/

        if(groupProductItemUpdate.getExistencia()==newCantidad)
        {

        }else
        {
            int cantidad_=0;
            String concepto="";
            if(groupProductItemUpdate.getExistencia()>newCantidad )
            {
                cantidad_=groupProductItemUpdate.getExistencia()-newCantidad;
                concepto="Ajuste";
            }else
            {
                cantidad_=newCantidad - groupProductItemUpdate.getExistencia();
                concepto="Entrada";
            }


            entradasSalidas entradasSalidasNew=new entradasSalidas();
            entradasSalidasNew.setIdProducto(groupProductItemUpdate.getId()+"");
            entradasSalidasNew.setCantidad(cantidad_);
            entradasSalidasNew.setConcepto(concepto);
            entradasSalidasNew.setFecha(Utils.getCurrentTimeCityDate());
            entradasSalidasNew.setPrecio(groupProductItemUpdate.getPrecio()+"");
            entradasSalidasNew.save();

            groupProductItemUpdate.setExistencia(newCantidad);

            mExistenciaNow.setText("");
            mExistenciaNew.setText("");
            mNombreProductoSelect.setText("");
            groupProductItemUpdate.save();

            groupProductItemUpdate=null;
        }

        Alerts.showToastMessage(this,"La existencia se actualizo correctamente");


        mSaveProduct.setEnabled(false);
        setInserData();
    }

    @Override
    public void ClickDelete(final productData groupProductItem) {

    }
    productData groupProductItemUpdate;
    @Override
    public void ClickEdit(final productData groupProductItem) {
        Alerts.showDialogWithCallback(Ajuste.this, "¿Deseas modificar a existencia del producto  "+groupProductItem.getClave()+"?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                groupProductItemUpdate=groupProductItem;
                mSaveProduct.setText("Modificar");
                mExistenciaNow.setText(groupProductItem.getExistencia()+"");
                mExistenciaNew.setText(groupProductItem.getExistencia()+"");
                mNombreProductoSelect.setText(groupProductItem.getClave());
                mSaveProduct.setEnabled(true);

            }
        });
    }



    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
