package ecubi.com.pventa.view.activity.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.widget.EditText;

import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.library.Alerts;
import ecubi.com.pventa.library.Utils;
import ecubi.com.pventa.model.sugar.user;
import ecubi.com.pventa.view.activity.menu.MainMenu;

public class login extends AppCompatActivity {

    @Bind(R.id.login_username) EditText userNameLogin;
    @Bind(R.id.login_password) EditText passwordLogin;
    ProgressDialog progressDialog=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if(Utils.detectIsMobil(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_login_portrait);
        }else
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_login_landscape);
        }
        /*inicia ButterKnife*/
        ButterKnife.bind(this);
        initialize_loader();
    }


    /*
    * Summary: inicializa cargador*/
    public void initialize_loader()
    {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMax(100);
        progressDialog.setMessage("Ingresando . . .");
        progressDialog.setTitle("Inicio de sesión");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

    }


    @Override
    protected void onResume() {
        super.onResume();
        SugarContext.init(this);/*inicia la DB*/
// create table if not exists
        SchemaGenerator schemaGenerator = new SchemaGenerator(this);
        schemaGenerator.createDatabase(new SugarDb(this).getDB());
        /*valida si existen usuarios de lo contrario agregalos a la base*/
        if(user.count(user.class)==0)
        {
            user userNew=new user();
            userNew.setEmail("admin@gmail.com");
            userNew.setUserName("admin");
            userNew.setPassword("admin");
            userNew.setAdmin(true);
            userNew.save();
        }


        /*Intent inteMainMenu = new Intent(this, MainMenu.class);
        inteMainMenu.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(inteMainMenu);*/
    }

    @OnClick(R.id.login_signin)
    public void login_signin()
    {
        /*valida los datos para poder loguearte*/

        String userName=userNameLogin.getText().toString().trim();
        String password=passwordLogin.getText().toString().trim();

        if(userName.isEmpty() || password.isEmpty())
        {
            Alerts.showToastMessage(this,"Necesitas colocar usuario y contraseña");
            return;
        }

        progressDialog.show();
        List<user> ListUser= user.listAll(user.class);
        boolean loginActive=false;
        for(user userItem:ListUser)
        {
            if(userItem.getUserName().equals(userName) && userItem.getPassword().equals(password))
            {
                loginActive=true;
                break;
            }
        }
        if(loginActive) {
            Intent inteMainMenu = new Intent(this, MainMenu.class);
            inteMainMenu.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(inteMainMenu);
        }else
        {
            Alerts.showToastMessage(this,"Usuario o contraseña incorrectos");
        }
        progressDialog.dismiss();
    }

}
