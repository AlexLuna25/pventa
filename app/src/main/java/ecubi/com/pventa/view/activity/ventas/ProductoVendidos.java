package ecubi.com.pventa.view.activity.ventas;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ecubi.com.appecubi.R;
import ecubi.com.pventa.library.Alerts;
import ecubi.com.pventa.model.sugar.entradasSalidas;
import ecubi.com.pventa.view.adapter.AdapterViewProductVendido;

public class ProductoVendidos extends AppCompatActivity {


    @Bind(R.id.ListViewData)
    RecyclerView mListViewData;

    @Bind(R.id.periodo_uno)
    EditText periodo_uno_;

    @Bind(R.id.periodo_dos)
    EditText periodo_dos_;

    DatePickerDialog mDatePickerDialog;
    DatePickerDialog mDatePickerDialog2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto_vendidos);


        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        setTitle("Productos Vendidos");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        mListViewData.setLayoutManager(linearLayoutManager);
        mListViewData.setHasFixedSize(true);
        periodo_uno_.setFocusable(false);
        periodo_dos_.setFocusable(false);


        setProductosVendidosAdapter(false);

        Calendar mCalendar = Calendar.getInstance();

        mDatePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(
                            DatePickerDialog datePickerDialog, int year, int month,
                            int day) {
                        String mDate = String.valueOf(year) + "/" + String.valueOf(month + 1)
                                + "/" + String.valueOf(day);


                        try {
                            SimpleDateFormat df=new SimpleDateFormat("yyyy/MM/dd");
                            Date dateSelect = df.parse(mDate);
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                            Date dateNow_ = df.parse(dateFormat.format(new Date()));
                            dateNow_.setDate(dateNow_.getDate()+1);

                            String mDate_ = String.valueOf(day) + "/" + String.valueOf(month + 1)
                                    + "/" + String.valueOf(year);
                            String month_= String.valueOf(month + 1);
                            String day_= String.valueOf(day);
                            month_=month_.length()==1?("0"+month_):month_;
                            day_=day_.length()==1?("0"+day_):day_;
                            mDate_ = day_+ "-" +month_
                                    + "-" + String.valueOf(year) ;



                            periodo_uno_.setText(mDate_);


                        } catch (ParseException e) {

                        }

                    }
                }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH), true);



        mDatePickerDialog2 = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(
                            DatePickerDialog datePickerDialog, int year, int month,
                            int day) {
                        String mDate = String.valueOf(year) + "/" + String.valueOf(month + 1)
                                + "/" + String.valueOf(day);


                        try {
                            SimpleDateFormat df=new SimpleDateFormat("yyyy/MM/dd");
                            Date dateSelect = df.parse(mDate);
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                            Date dateNow_ = df.parse(dateFormat.format(new Date()));
                            dateNow_.setDate(dateNow_.getDate()+1);

                            String mDate_ = String.valueOf(day) + "/" + String.valueOf(month + 1)
                                    + "/" + String.valueOf(year);
                            String month_= String.valueOf(month + 1);
                            String day_= String.valueOf(day);
                            month_=month_.length()==1?("0"+month_):month_;
                            day_=day_.length()==1?("0"+day_):day_;
                            mDate_ = day_+ "-" +month_
                                    + "-" + String.valueOf(year) ;



                            periodo_dos_.setText(mDate_);


                        } catch (ParseException e) {

                        }

                    }
                }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH), true);


    }

    @OnClick(R.id.consultar)
    public void consultar()
    {
        if(periodo_uno_.getText().toString().trim().isEmpty() || periodo_dos_.getText().toString().trim().isEmpty())
        {
            Alerts.showToastMessage(this,"Necesitas seleccionar las fechas");
            return;
        }

        setProductosVendidosAdapter(true);
    }

    @OnClick(R.id.periodo_dos)
    public void periodo_dos()
    {
        mDatePickerDialog2.show(getSupportFragmentManager(),"datePicker");
    }

    @OnClick(R.id.periodo_uno)
    public void periodo_uno()
    {
        mDatePickerDialog.show(getSupportFragmentManager(),"datePicker");
    }

    public void setProductosVendidosAdapter(boolean filters)
    {
        List<entradasSalidas> ListentradasSalidas=entradasSalidas.find(entradasSalidas.class,"concepto=?","Salida");


        if(ListentradasSalidas.size()>0)
        {
            List<entradasSalidas> ListentradasSalida__=new ArrayList<>();
            for(entradasSalidas entradasSalidasItem:ListentradasSalidas)
            {
                if(filters) {
                    String dtStart = periodo_uno_.getText().toString()+  " 00:00:00";
                    String dtEnd = periodo_dos_.getText().toString()+ " 24:00:00";
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    try {
                        Date date1 = format.parse(dtStart);
                        Date date2 = format.parse(dtEnd);
                        Date date3 = format.parse(entradasSalidasItem.getFecha());
                        if (date3.after(date1) && date2.after(date3)) {
                            ListentradasSalida__.add(entradasSalidasItem);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }else {
                    ListentradasSalida__.add(entradasSalidasItem);
                }
            }


            AdapterViewProductVendido AdapterViewProductVendidotem=new AdapterViewProductVendido(this,ListentradasSalida__);
            mListViewData.setAdapter(AdapterViewProductVendidotem);
        }else
        {
            mListViewData.setAdapter(null);
        }
    }

    /*metodo para obtenre el click en back*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
