package ecubi.com.pventa.model.sugar;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by pro on 22/01/18.
 */
@Table
public class ventaProductos extends SugarRecord {

    public String ticketVenta;
    public String cantidadProductos;
    public String precioProductos;
    public String precioUnicoProducto;

    public ventaProductos()
    {

    }

    public void setTicketVenta(String ticketVenta)
    {
        this.ticketVenta=ticketVenta;
    }

    public String getTicketVenta()
    {
        return this.ticketVenta;
    }

    public void setCantidadProductos(String cantidadProductos)
    {
        this.cantidadProductos=cantidadProductos;
    }

    public String getCantidadProductos()
    {
        return this.cantidadProductos;
    }

    public void setPrecioProductos(String precioProductos)
    {
        this.precioProductos=precioProductos;
    }

    public String getPrecioProductos()
    {
        return this.precioProductos;
    }


    public void setPrecioUnicoProducto(String precioUnicoProducto)
    {
        this.precioUnicoProducto=precioUnicoProducto;
    }

    public String getPrecioUnicoProducto()
    {
        return this.precioUnicoProducto;
    }


}
