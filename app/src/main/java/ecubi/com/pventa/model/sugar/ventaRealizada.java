package ecubi.com.pventa.model.sugar;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by pro on 22/01/18.
 */
@Table
public class ventaRealizada extends SugarRecord {

    public String ticketVenta;
    public String cantidadProductos;
    public String precioProductos;
    public String fechaVenta;
    public String totalPagar;
    public boolean cancelada;
    public boolean corteCaja;
    public String idCorteCaja;


    public ventaRealizada()
    {

    }

    public void setTicketVenta(String ticketVenta)
    {
        this.ticketVenta=ticketVenta;
    }

    public String getTicketVenta()
    {
        return this.ticketVenta;
    }

    public void setCantidadProductos(String cantidadProductos)
    {
        this.cantidadProductos=cantidadProductos;
    }

    public String getCantidadProductos()
    {
        return this.cantidadProductos;
    }

    public void setPrecioProductos(String precioProductos)
    {
        this.precioProductos=precioProductos;
    }

    public String getPrecioProductos()
    {
        return this.precioProductos;
    }

    public void setFechaVenta(String fechaVenta)
    {
        this.fechaVenta=fechaVenta;
    }


    public String getFechaVenta()
    {
        return this.fechaVenta;
    }


    public void setTotalPagar(String totalPagar)
    {
        this.totalPagar=totalPagar;
    }

    public String getTotalPagar()
    {
        return this.totalPagar;
    }


    public void setCancelada(boolean cancelada)
    {
        this.cancelada=cancelada;
    }


    public boolean getCancelada()
    {
        return this.cancelada;
    }


    public void setCorteCaja(boolean corteCaja)
    {
        this.corteCaja=corteCaja;
    }

    public boolean getCorteCaja()
    {
        return this.corteCaja;
    }

    public void setIdCorteCaja(String idCorteCaja)
    {
        this.idCorteCaja=idCorteCaja;
    }
    public String getIdCorteCaja()
    {
        return this.idCorteCaja;
    }

}
