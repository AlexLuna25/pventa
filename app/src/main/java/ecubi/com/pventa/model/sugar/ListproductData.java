package ecubi.com.pventa.model.sugar;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by pro on 19/01/18.
 */
@Table
public class ListproductData extends SugarRecord{

    private productData producto;
    private int cantidad;


    public ListproductData()
    {

    }


    public void setProducto(productData producto)
    {
        this.producto=producto;
    }

    public productData getProducto()
    {
        return this.producto;
    }

    public void setCantidad(int cantidad)
    {
        this.cantidad=cantidad;
    }

    public int getCantidad()
    {
        return this.cantidad;
    }

}
