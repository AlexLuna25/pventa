package ecubi.com.pventa.model.sugar;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by pro on 18/01/18.
 */
@Table
public class user extends SugarRecord{

    private String userName;
    private String password;
    private String email;
    private boolean admin;



    public user()
    {

    }


    public void setUserName(String userName)
    {
        this.userName=userName;
    }

    public String getUserName()
    {
        return this.userName;
    }


    public void setPassword(String password)
    {
        this.password=password;
    }


    public String getPassword()
    {
        return this.password;
    }



    public void setEmail(String email)
    {
        this.email=email;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setAdmin(boolean admin)
    {
        this.admin=admin;
    }

    public boolean getAdmin()
    {
        return this.admin;
    }
}
