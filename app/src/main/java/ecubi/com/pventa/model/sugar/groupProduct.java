package ecubi.com.pventa.model.sugar;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by pro on 18/01/18.
 */
@Table
public class groupProduct extends SugarRecord {

    private String name;


    public groupProduct()
    {

    }

    public void setName(String name)
    {
        this.name=name;
    }

    public String getName()
    {
        return this.name;
    }


}
