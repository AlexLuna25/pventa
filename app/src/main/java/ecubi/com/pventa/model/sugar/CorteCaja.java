package ecubi.com.pventa.model.sugar;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by pro on 22/01/18.
 */
@Table
public class CorteCaja extends SugarRecord{

    public String fecha;

    public CorteCaja()
    {

    }

    public void setFecha(String fecha)
    {
        this.fecha=fecha;
    }

    public String getFecha()
    {
        return this.fecha;
    }
}
