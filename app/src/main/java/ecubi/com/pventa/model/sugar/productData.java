package ecubi.com.pventa.model.sugar;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by pro on 19/01/18.
 */
@Table
public class productData extends SugarRecord{

    private String imagen;
    private String clave;
    private String descripcion;
    private String codigoBarras;
    private String precio;
    private String idGrupo;
    private String claveSat;
    private String fechaUltimaCompra;
    private String fechaUltimaVenta;
    private int existencia;


    public productData()
    {

    }


    public void setImagen(String imagen)
    {
        this.imagen=imagen;
    }

    public String getImagen()
    {
        return this.imagen;
    }


    public void setClave(String clave)
    {
        this.clave=clave;
    }

    public String getClave()
    {
        return this.clave;
    }


    public void setDescripcion(String descripcion)
    {
        this.descripcion=descripcion;
    }

    public String getDescripcion()
    {
        return this.descripcion;
    }


    public void setPrecio(String precio)
    {
        this.precio=precio;
    }

    public String getPrecio()
    {
        return this.precio;
    }

    public void setIdGrupo(String idGrupo)
    {
        this.idGrupo=idGrupo;
    }

    public  String getIdGrupo()
    {
        return this.idGrupo;
    }


    public void setClaveSat(String claveSat)
    {
        this.claveSat=claveSat;
    }

    public String getClaveSat()
    {
        return this.claveSat;
    }


    public void setFechaUltimaCompra(String fechaUltimaCompra)
    {
        this.fechaUltimaCompra=fechaUltimaCompra;
    }

    public String getFechaUltimaCompra()
    {
        return this.fechaUltimaCompra;
    }

    public void setFechaUltimaVenta(String fechaUltimaVenta)
    {
        this.fechaUltimaVenta=fechaUltimaVenta;
    }

    public String getFechaUltimaVenta()
    {
        return this.fechaUltimaVenta;
    }


    public void setCodigoBarras(String codigoBarras)
    {
        this.codigoBarras=codigoBarras;
    }

    public String getCodigoBarras()
    {
        return this.codigoBarras;
    }


    public void setExistencia(int existencia)
    {
        this.existencia=existencia;
    }

    public int getExistencia()
    {
        return this.existencia;
    }

    @Override
    public String toString() {
        return clave;
    }

}
