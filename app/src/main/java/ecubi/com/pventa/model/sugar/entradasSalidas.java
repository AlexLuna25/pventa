package ecubi.com.pventa.model.sugar;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by pro on 19/01/18.
 */
@Table
public class entradasSalidas extends SugarRecord{

    private String idProducto;
    private int cantidad;
    private String concepto;
    private String precio;
    private String fecha;


    public entradasSalidas()
    {

    }

    public void setIdProducto(String idProducto)
    {
        this.idProducto=idProducto;
    }

    public String getIdProducto()
    {
        return this.idProducto;
    }


    public void setCantidad(int cantidad)
    {
        this.cantidad=cantidad;
    }

    public int getCantidad()
    {
        return this.cantidad;
    }


    public void setConcepto(String concepto)
    {
        this.concepto=concepto;
    }

    public String getConcepto()
    {
        return this.concepto;
    }

    public void setFecha(String fecha)
    {
        this.fecha=fecha;
    }

    public String getFecha()
    {
        return this.fecha;
    }



    public void setPrecio(String precio)
    {
        this.precio=precio;
    }

    public String getPrecio()
    {
        return this.precio;
    }
}
